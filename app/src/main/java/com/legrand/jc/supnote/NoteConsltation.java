package com.legrand.jc.supnote;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class NoteConsltation extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_consltation);

        ((TextView)findViewById(R.id.tv_title)).setText(getIntent().getStringExtra(MainActivity.ARG_TITLE));
        ((TextView)findViewById(R.id.tv_description)).setText(getIntent().getStringExtra(MainActivity.ARG_DESCRIPTION));

        ((Button) findViewById(R.id.btn_delete)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(MainActivity.CODE_RESULT_CONSULT, new Intent().putExtra(MainActivity.ARG_POSITION, getIntent().getIntExtra(MainActivity.ARG_POSITION, -1)));
                finish();
            }
        });
    }
}
