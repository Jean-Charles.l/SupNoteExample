package com.legrand.jc.supnote;

import android.content.Intent;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    public static final int CODE_RESULT_CREATION = 42;
    public static final int CODE_RESULT_CONSULT = 4;
    private ArrayList<OneNote> mListNotes = new ArrayList<>();
    public final static String ARG_TITLE = "title";
    public final static String ARG_DESCRIPTION = "description";
    public final static String ARG_POSITION = "position";
    private MyAdapteur mAdapter;
    private RecyclerView mRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ((Button) findViewById(R.id.btn_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getApplicationContext(), NoteCreationActivity.class), CODE_RESULT_CREATION);
            }
        });

        mAdapter = new MyAdapteur(mListNotes);
        mRecycler = (RecyclerView) findViewById(R.id.recycler);
        mRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecycler.setAdapter(mAdapter);

        String data = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("allNotes", "");
        mListNotes.clear();
        if(!data.isEmpty()){
            try {
                JSONArray json = new JSONArray(data);
                for(int i = 0; i < json.length(); i++){
                    JSONObject obj = json.getJSONObject(i);
                    mListNotes.add(new OneNote(obj.getString(ARG_TITLE), obj.getString(ARG_DESCRIPTION)));
                }

                mAdapter = new MyAdapteur(mListNotes);
                mRecycler.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        JSONArray array = new JSONArray();
        for(OneNote note : mListNotes){
            JSONObject n = new JSONObject();
            try {
                n.put(ARG_TITLE, note.mTitle);
                n.put(ARG_DESCRIPTION, note.mDescription);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            array.put(n);
        }

        PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).edit().putString("allNotes", array.toString()).apply();
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == CODE_RESULT_CREATION){
            if(data != null && data.getStringExtra(ARG_TITLE) != null){
                mListNotes.add(new OneNote(data.getStringExtra(ARG_TITLE), data.getStringExtra(ARG_DESCRIPTION)));
                mAdapter.notifyDataSetChanged();
            }
        } else if(requestCode == CODE_RESULT_CONSULT){
            if(data != null && data.getIntExtra(ARG_POSITION, -1) != -1){
                mAdapter.mList.remove(data.getIntExtra(ARG_POSITION, 0));
                mAdapter.notifyDataSetChanged();
            }
        }

    }

    public class MyAdapteur extends RecyclerView.Adapter<ViewHolder> {

        ArrayList<OneNote> mList;

        MyAdapteur(ArrayList<OneNote> list){
            mList = list;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder(getLayoutInflater().inflate(R.layout.item_list, null));
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            holder.mTitle.setText(mList.get(position).mTitle);
            holder.mContainer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), NoteConsltation.class)
                            .putExtra(ARG_POSITION, position)
                            .putExtra(ARG_TITLE, mListNotes.get(position).mTitle)
                            .putExtra(ARG_DESCRIPTION, mListNotes.get(position).mDescription);
                    startActivityForResult(i, CODE_RESULT_CONSULT);
                }
            });
        }

        @Override
        public int getItemCount() {
            return mList.size();
        }
    }


    private class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mTitle;
        private View mContainer;
        public ViewHolder(View itemView) {
            super(itemView);
            mTitle = (TextView) itemView.findViewById(R.id.tv_item_title);
            mContainer = itemView;
        }
    }

    public class OneNote {
        public String mTitle;
        public String mDescription;

        public OneNote(String title, String desc) {
            mTitle = title;
            mDescription = desc;
        }
    }
}
