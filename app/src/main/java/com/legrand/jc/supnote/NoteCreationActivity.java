package com.legrand.jc.supnote;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NoteCreationActivity extends AppCompatActivity {

    public EditText mEdTitle;
    public EditText mEdDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_creation);

        mEdTitle = (EditText) findViewById(R.id.ed_title);
        mEdDescription = (EditText) findViewById(R.id.ed_description);

        ((Button) findViewById(R.id.btn_add)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.putExtra(MainActivity.ARG_TITLE, mEdTitle.getText().toString());
                i.putExtra(MainActivity.ARG_DESCRIPTION, mEdDescription.getText().toString());
                NoteCreationActivity.this.setResult(MainActivity.CODE_RESULT_CREATION, i);
                NoteCreationActivity.this.finish();
            }
        });

        ((Button) findViewById(R.id.btn_cancel)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
